import antfu from '@antfu/eslint-config'

export default antfu(
  {
    unocss: true,
    formatters: true,
    // `.eslintignore` is no longer supported in Flat config, use `ignores` instead
    ignores: [
      '**/dist/',
      '**/dist*/',
      '**/generated/',
      '**/public/',
      'pnpm-lock.yaml',
    ],
  },
  {
    rules: {
      // 强制不可使用全局process，必须先require，这里关闭
      'node/prefer-global/process': 'off',
      // 会把 @ts-ignore => @ts-expect-error，这里关闭
      'ts/prefer-ts-expect-error': 'off',
      // @ts-ignore 之后是否必须填写原因，这里关闭
      'ts/ban-ts-comment': 'off',
      // 强制使用 '==='，这里关闭
      'eqeqeq': 'off',
      'vue/eqeqeq': 'off',
      // 未使用的变量会报错，这里关闭
      'unused-imports/no-unused-vars': 'off',
      // 在变量或方法声明之前调用会报错，这里关闭
      'ts/no-use-before-define': 'off',
      // 禁止console，这里关闭
      'no-console': 'off',
      // 自定义的vue emits事件必须使用小驼峰，这里关闭
      'vue/custom-event-name-casing': 'off',
      // 三元表达式中条件部分（test）和结果部分（consequent）之间必须添加换行符，这里关闭
      'style/multiline-ternary': 'off',
      // 禁止空的数组作为参数占位符，这里关闭
      'no-empty-pattern': 'off',
      // 禁止使用未赋值的三元表达式，这里关闭
      'no-unused-expressions': 'off',
      // 禁止导出非const定义的变量，这里关闭
      'import/no-mutable-exports': 'off',
      // 禁止在switch case中声明变量，这里关闭
      'no-case-declarations': 'off',
      // 正则表达式文字不必要地包装在“RegExp”构造函数中，这里关闭
      'prefer-regex-literals': 'off',
      // 请使用rest参数而不是“arguments”，这里关闭
      'prefer-rest-params': 'off',
      // 不强制要求使用花括号
      'curly': 'off',
      // 禁止使用分号
      'semi': ['error', 'never'],
      // 是否禁止使用`immediate && start()`写法，这里关闭
      'ts/no-unused-expressions': 'off',
      // 是否禁止在promise中使用await
      'no-async-promise-executor': 'off',
    },
  },
  {
    files: ['**/*.vue', '**/*.ts', '**/*.d.ts'],
    rules: {
      'no-extra-semi': 'error',
    },
  },
)
