import { defineConfig } from 'unocss'
import transformerVariantGroup from '@unocss/transformer-variant-group'
import transformerDirectives from '@unocss/transformer-directives'
import presetIcons from '@unocss/preset-icons'
import presetUno from '@unocss/preset-uno'

export default defineConfig({
  // ...UnoCSS options
  presets: [
    presetUno({ dark: 'class', attributify: false }),
    presetIcons({
      prefix: '',
    }),
  ],
  transformers: [transformerVariantGroup(), transformerDirectives()],
})
