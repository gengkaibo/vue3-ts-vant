import type { RouteRecordRaw } from 'vue-router'

/* 系统内置常量路由 */
export const routeTree: RouteRecordRaw[] = [
  {
    path: '/',
    redirect: '/home',
    name: 'Root',
    children: [
      {
        path: 'home',
        name: 'home',
        component: () => import('@/views/home.vue'),
        meta: {
          title: '主页',
        },
      },
      {
        path: 'goods',
        name: 'goods',
        component: () => import('@/views/goods.vue'),
        meta: {
          title: '商品',
        },
      },
      {
        path: 'my',
        name: 'my',
        component: () => import('@/views/my.vue'),
        meta: {
          title: '我的',
        },
      },
    ],
  },
  {
    path: '/redirect/:path(.*)',
    name: 'Redirect',
    component: () => import('@/views/redirect/index.vue'),
    meta: {
      title: '重定向',
    },
  },
  {
    path: '/404',
    component: () => import('@/views/error/404.vue'),
    name: 'NoFind',
    meta: {
      title: '404',
    },
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/404',
    name: 'PathMatch404',
    meta: {
      title: '404',
    },
  },
]
