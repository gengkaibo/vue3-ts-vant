// 事件总线第三方库：
import mitt from 'mitt'

const $globalEventBus = mitt()
export {
  $globalEventBus,
}
