/**
 * 等待一段时间
 * @param time 单位ms
 */
export async function sleep(time = 0) {
  await new Promise((resolve) => {
    setTimeout(() => {
      resolve(true)
    }, time)
  })
}
