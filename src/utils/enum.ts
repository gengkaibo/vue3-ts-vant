/**
 * 枚举工具类
 * @module Enum
 * @Date 2022年2月22日14:44:44
 * @updateDate 2022年3月14日12:56:05
 * @Author 沉淀
 * @version：0.0.1
 */
export class Enum {
  [k: string]: any
  list: EnumItem[]
  constructor(list: EnumItem[] = [{ label: '', value: '' }]) {
    this.list = list
    this.list.forEach((element: EnumItem) => {
      this[element.label] = element.value
    })
    this.list.forEach((element: EnumItem) => {
      this[`${element.value}`] = element.label
    })
  }

  /**
   * 根据label获取对应value
   */
  getEnumValue(label: EnumLabel) {
    const index = this.list.findIndex(item => item.label == label)
    return index > -1 ? this.list[index].value : null
  }

  /**
   * 根据value获取对应label
   */
  getEnumLabel(value: EnumValue) {
    const index = this.list.findIndex(item => item.value == value)
    return index > -1 ? this.list[index].label : null
  }

  /**
   * 根据label或者value获取对应对象
   */
  getTarget(param: EnumLabel | EnumValue): EnumItem | null {
    const index = this.list.findIndex(
      item => item.value == param || item.label == param,
    )
    return index > -1 ? this.list[index] : null
  }

  /**
   * 获取所有的label集合
   */
  labels(): EnumLabel[] {
    const labels = this.list.map((item) => {
      return item.label
    })
    return labels
  }

  /**
   * 获取所有的value集合
   */
  values(): EnumValue[] {
    const values = this.list.map((item) => {
      return item.value
    })
    return values
  }
}

export type EnumLabel = string
export type EnumValue = string | number
export interface EnumItem<T = any> {
  label: EnumLabel
  value: EnumValue
  meta?: T
}
