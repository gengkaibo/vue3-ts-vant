import type { AxiosRequestConfig } from 'axios'
/**
 * 'application/json;charset=UTF-8'
 * | 'application/x-www-form-urlencoded;charset=UTF-8'
 * | 'multipart/form-data;charset=UTF-8'
 */
export enum ContentType {
  // json
  JSON = 'application/json;charset=UTF-8',
  // form-data qs
  FORM_URLENCODED = 'application/x-www-form-urlencoded;charset=UTF-8',
  // form-data  upload
  FORM_DATA = 'multipart/form-data;charset=UTF-8',
}
/**
 * request method
 */
export enum RequestMethod {
  get = 'get',
  post = 'post',
  put = 'put',
  delete = 'delete',
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  DELETE = 'DELETE',
}
export interface MyRequestConfig extends AxiosRequestConfig {
  headers?: {
    'Content-Type'?: ContentType
    [x: string]: any
  }
}

/**
 * 将params转换为地址栏传参的形式
 * @param params - requestParams
 * @returns 返回地址栏拼接参数
 */
export function tansParams(params: Record<string, any>) {
  let result = ''
  for (const propName of Object.keys(params)) {
    const value = params[propName]
    const part = `${encodeURIComponent(propName)}=`
    if (value !== null && value !== '' && typeof value !== 'undefined') {
      if (typeof value === 'object') {
        for (const key of Object.keys(value)) {
          if (
            value[key] !== null
            && value[key] !== ''
            && typeof value[key] !== 'undefined'
          ) {
            const params = `${propName}[${key}]`
            const subPart = `${encodeURIComponent(params)}=`
            result += `${subPart + encodeURIComponent(value[key])}&`
          }
        }
      }
      else {
        result += `${part + encodeURIComponent(value)}&`
      }
    }
  }
  return result
}
