import type { MyRequestConfig } from './helpers'
import { RequestMethod } from './helpers'
import { axiosInstance, baseURL, service } from './service'

const axios = {
  request: service.request,

  get: <T = any>(option: MyRequestConfig) => {
    return service.request<T>({ method: RequestMethod.get, ...option })
  },
  post: <T = any>(option: MyRequestConfig) => {
    return service.request<T>({ method: RequestMethod.post, ...option })
  },
  delete: <T = any>(option: MyRequestConfig) => {
    return service.request<T>({ method: RequestMethod.delete, ...option })
  },
  put: <T = any>(option: MyRequestConfig) => {
    return service.request<T>({ method: RequestMethod.put, ...option })
  },
  cancelRequest: (url: string | string[]) => {
    return service.cancelRequest(url)
  },
  cancelAllRequest: () => {
    return service.cancelAllRequest()
  },
}

export { axiosInstance, baseURL, HTTP, HTTP as default }
