import axios from '@/api/axios'

/**
 * 效能-获取所有卫星
 */
export function getSatellite() {
  return axios.post({
    url: `/sateConfig/orbitPrediction/select`,
  })
}
/**
 * 效能-获取所有碎片
 */
export function getPuzzle() {
  return axios.post({
    url: `/sateConfig/spaceDebris/select`,
  })
}

/**
 * 效能-根据条件获取卫星碎片
 * @property shardId - 碎片编号
 * @property shardName - 碎片名称
 * @property altitudeMin - 最低高度
 * @property altitudeMax - 最高高度
 * @property dipAngleMin - 最小倾角
 * @property dipAngleMax - 最大倾角
 */
export function getPuzzleByCondition(params: {
  shardId?: string
  shardName?: string
  altitudeMin?: number
  altitudeMax?: number
  dipAngleMin?: number
  dipAngleMax?: number
}) {
  return axios.post({
    url: `/spaceDebris/filtrate`,
    data: params,
  })
}

/**
 * 孪生-获取卫星的列表
 */
export function getSateList() {
  return axios.post({
    url: `/sateConfig/orbitPrediction/select`,
  })
}

/**
 * 孪生-获取卫星的列表
 */
export function analysisAlarm(params: {
  id: string
  excludeTle: number
  beginTime: string
  endTime: string
}) {
  return axios.post({
    url: `/sateConfig/crash/init`,
    data: params,
  })
}
