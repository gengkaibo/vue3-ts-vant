// 引入适配
import 'amfe-flexible'

// 引入unocss.css
import 'virtual:uno.css'

// vant 按需引入-toast轻提示
import 'vant/es/toast/style'

// 桌面端适配
import '@vant/touch-emulator'

// 全局样式
import './assets/styles/index.less'
import { createApp } from 'vue'
import { store } from './store'
import router from './router'
import App from './App.vue'

const app = createApp(App)

app.use(store).use(router).mount('#app')
