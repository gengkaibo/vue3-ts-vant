import { Enum } from '@/utils/enum'

const params = [
  {
    label: '开启',
    value: '1',
  },
  {
    label: '暂停',
    value: '2',
  },
] as const

/**
 * 状态枚举
 [
  {
    label: '开启',
    value: '1',
  },
  {
    label: '暂停',
    value: '2',
  },
]
 * @returns Enum实例对象
 */
// @ts-ignore
export const statusEnum = new Enum(params)
