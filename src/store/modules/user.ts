import { defineStore } from 'pinia'
import { store } from '../index'
/**
 * 登录获取的用户信息
 */
export interface LoginInfo {
  permissions: string[]
  roles: string[]
  user: any
}
/**
 * userStore state类型
 */
export interface UserState {
  token: string
  userInfo?: any
  loginInfo?: LoginInfo
  rememberMe: boolean
}

export const useUserStore = defineStore('pinia-user', {
  state: (): UserState => {
    return {
      token: '',
      rememberMe: true,
      userInfo: undefined,
      loginInfo: undefined,
    }
  },
  getters: {
    getToken(): string {
      return this.token
    },
    getUserInfo(): any {
      return this.userInfo
    },
    getRememberMe(): boolean {
      return this.rememberMe
    },
    getLoginInfo(): LoginInfo | undefined {
      return this.loginInfo
    },
  },
  actions: {
    setToken(token: string) {
      this.token = token
    },
    setUserInfo(userInfo?: any) {
      this.userInfo = userInfo
    },
    setRememberMe(rememberMe: boolean) {
      this.rememberMe = rememberMe
    },
    setLoginInfo(loginInfo: LoginInfo | undefined) {
      this.loginInfo = loginInfo
    },
    logoutReset() {
      this.$reset()
    },
  },
  // 自动缓存 default：localStorage
  persist: true,
})

export function useUserStoreWithOut() {
  return useUserStore(store)
}
