import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

const store = createPinia()

// 注意：如果在非setup上下文调用store，请注意调用的执行时机不可在store初始化之前，否则persist将无法生效
store.use(piniaPluginPersistedstate)

export { store }
