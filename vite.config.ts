import { resolve } from 'node:path'
import type { ConfigEnv, UserConfig } from 'vite'
import { loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import UnoCSS from 'unocss/vite'
import autoImport from 'unplugin-auto-import/vite'
import components from 'unplugin-vue-components/vite'
import { VantResolver } from '@vant/auto-import-resolver'
import { createHtmlPlugin } from 'vite-plugin-html'
import viteCompression from 'vite-plugin-compression'
import autoprefixer from 'autoprefixer'
import postCssPxToRem from 'postcss-pxtorem'
import viewport from 'postcss-mobile-forever'
import { viteVConsole } from 'vite-plugin-vconsole'

function pathResolve(dir: string) {
  return resolve(process.cwd(), '.', dir)
}

// https://vitejs.dev/config/
export default ({ mode }: ConfigEnv): UserConfig => {
  const env = loadEnv(mode, process.cwd()) as ImportMetaEnv
  const outDir = env.VITE_APP_CONFIG_OUTDIR
  return {
    base: env.VITE_APP_CONFIG_BASE,
    // 路径重定向
    resolve: {
      alias: [
        {
          find: '@',
          replacement: pathResolve('src'),
        },
      ],
    },
    server: {
      open: true,
      port: 5200,
      hmr: {
        overlay: false,
      },
      host: '0.0.0.0',
      proxy: {
        [env.VITE_APP_PROXY]: {
          target: env.VITE_APP_BASE_API, // 代理接口
          changeOrigin: true,
          rewrite: path => path.replace(new RegExp(`^${env.VITE_APP_PROXY}`), ''),
        },
      },
    },
    plugins: [
      vue(),
      vueJsx(),
      UnoCSS(),
      components({
        resolvers: [VantResolver()],
        dirs: ['src/components'],
        dts: 'typings/components.d.ts',
      }),
      // 自动全局导入，组件里面可以无需再引用
      autoImport({
        imports: [
          'vue', // 全局自动导入vue
          'vue-router', // 全局自动导入路由插件
          'pinia', // 全局自动导入pinia
          '@vueuse/core', // 全局自动导入vueuse
        ],
        dts: 'typings/auto-imports.d.ts', // 生成 `auto-imports.d.ts` 全局声明
      }),
      // html注入
      createHtmlPlugin({
        inject: {
          data: {
            VITE_APP_TITLE: env.VITE_APP_TITLE,
          },
        },
      }),
      // 开启gzip压缩
      viteCompression({
        verbose: true,
        disable: false,
        threshold: 1024 * 10, // 跳过小于10kb的文件
        algorithm: 'gzip',
        ext: '.gz',
      }),
      // 开发调试辅助
      // VueDevTools(),
      viteVConsole({
        entry: [pathResolve('src/main.ts')],
        enabled: true,
        config: {
          log: {
            maxLogNumber: 1000,
          },
          theme: 'light',
        },
        // https://github.com/vadxq/vite-plugin-vconsole/issues/21
        dynamicConfig: {
          theme: `document.documentElement.classList.contains('dark') ? 'dark' : 'light'`,
        },
        eventListener: `
          const targetElement = document.querySelector('html'); // 择要监听的元素
          const observerOptions = {
            attributes: true, // 监听属性变化
            attributeFilter: ['class'] // 只监听class属性变化
          };

          // 定义回调函数来处理观察到的变化
          function handleAttributeChange(mutationsList) {
            for(let mutation of mutationsList) {
              if (mutation.type === 'attributes' && mutation.attributeName === 'class') {
                if (window && window.vConsole) {
                  window.vConsole.dynamicChange.value = new Date().getTime();
                }
              }
            }
          }

          // 创建观察者实例并传入回调函数
          const observer = new MutationObserver(handleAttributeChange);

          // 开始观察目标元素
          observer.observe(targetElement, observerOptions);

          // 当不再需要观察时，停止观察
          // observer.disconnect();
        `,
      }),
    ],
    css: {
      postcss: {
        plugins: [
          autoprefixer(),
          viewport({
            appSelector: '#app',
            viewportWidth: 375,
            maxDisplayWidth: 600,
            rootContainingBlockSelectorList: [
              'van-tabbar',
              'van-popup',
            ],
          }),
          /* postCssPxToRem({
            // 配置在将px转化为rem时 1rem等于多少px(因为我们搭配使用了amfe-flexible插件 此处我们需要设置的值应是UI设计稿全屏基准宽度的十分之一)
            rootValue({ file }) {
              // 当UI设计稿的全屏基准宽度是750px时 此处设置的值为75 但项目中使用了vant组件库 vant的设计稿总宽度是375px 其十分之一应是37.5(需要区分设置)
              // return file.includes('vant') ? 37.5 : 75
              return 37.5
            },
            // 所有px均转化为rem
            propList: ['*'],
            // 若想设置部分样式不转化 可以在配置项中写出
            // eg: 除 border和font-size外 所有px均转化为rem
            // propList: ["*", "!border","!font-size"],
          }), */
        ],
      },
    },
    build: {
      outDir,
      minify: 'terser', // 如果需要用terser混淆，可打开这两行
      terserOptions: {
        compress: {
          drop_debugger: env.VITE_APP_DROP_DEBUGGER == 'true',
          drop_console: env.VITE_APP_DROP_CONSOLE == 'true',
        },
      },
    },
  }
}
