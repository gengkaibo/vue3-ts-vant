import type * as CesiumType from 'cesium'
import type * as KBE3DType from 'kbe3d-core'
import type { RouteLocationNormalizedLoaded, Router } from 'vue-router'
// 声明全局变量
declare global {
  const Cesium: typeof CesiumType
  const KBE3D: typeof KBE3DType
  const XTDCZ: any
  // 声明window挂载变量
  interface Window {
    earth: KBE3DType.Earth
    [x: string]: any
  }
}

declare module 'vue' {
  interface ComponentCustomProperties {
    $router: Router
    $route: RouteLocationNormalizedLoaded
  }
}
export {}
