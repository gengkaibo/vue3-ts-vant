/// <reference types="vite/client" />

interface ImportMetaEnv {
  /** 运行环境 */
  readonly VITE_APP_MODE: 'dev' | 'prod'
  /** 是否移除debugger */
  readonly VITE_APP_CONFIG_DROP_DEBUGGER: 'false' | 'true'
  /** 是否移除console */
  readonly VITE_APP_CONFIG_DROP_CONSOLE: 'false' | 'true'
  /** 用于配置公共基础路径 */
  readonly VITE_APP_CONFIG_BASE: string
  /** 打包输出文件夹 */
  readonly VITE_APP_CONFIG_OUTDIR: string
  /** 项目标题 */
  readonly VITE_APP_TITLE: string
  /** 项目标识 */
  readonly VITE_APP_SIGN: string
  /** 后端服务地址 */
  readonly VITE_APP_BASE_API: string
  /** 后端服务WebSocket地址 */
  readonly VITE_APP_WS_API: string
  /** 前端跨域代理 */
  readonly VITE_APP_BASE_API_PROXY: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
